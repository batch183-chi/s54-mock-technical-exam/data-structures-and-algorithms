let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print(){
	return collection;
}

function enqueue(name){
	collection.push(name);
	return collection;

}

function dequeue(){
	collection.splice(0,1);
	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length == 0){
		return true;
	}
	else{
		return false;
	}
}




module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};